#!/usr/bin/env node
'use strict';

// See https://git.coolaj86.com/coolaj86/acme-challenge-test.js
var tester = require('acme-challenge-test');
require('dotenv').config();

// Usage: node ./test.js example.com username xxxxxxxxx
var record = process.argv[2] || process.env.RECORD;
var challenger = require('./index.js').create({
	awsAccessKey: process.env.AWS_ACCESS_KEY,
	awsSecretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
	awsRegion: process.env.AWS_REGION,
	awsBucket: process.env.AWS_BUCKET
});

// The dry-run tests can pass on, literally, 'example.com'
// but the integration tests require that you have control over the domain
tester
	.testRecord('http-01', record, challenger)
	.then(function() {
		console.info('PASS', record);
	})
	.catch(function(e) {
		console.error(e.message);
		console.error(e.stack);
	});
