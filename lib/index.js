'use strict';
var promisify = require('util').promisify;
var request = require('@root/request');
request = promisify(request);
var aws4 = require('aws4');

var defaults = {
	awsRegion: 'us-east-1',
};

module.exports.create = function(config) {
	var awsAccessKey = config.awsAccessKey || null;
	var awsSecretAccessKey = config.awsSecretAccessKey || null;
	var awsRegion = config.awsRegion || defaults.awsRegion;
	var awsBucket = config.awsBucket || null;
	var awsHost = config.awsHost || 's3.'+awsRegion+'.amazonaws.com';

	if (awsAccessKey===null||awsSecretAccessKey===null){
		throw new Error('awsAccessKey and awsSecretAccessKey are required');
	}

	var AWSCredentials = {
		accessKeyId: awsAccessKey,
		secretAccessKey: awsSecretAccessKey
	};

	if (awsBucket===null){
		throw new Error('awsBucket is required');
	}

	return {
		init: function(opts) {
			return null;
		},

		set: function(data) {
			console.log('Add Key Auth URL');
			var ch = data.challenge;
			var signed = aws4.sign({
					host: awsHost,
					service: 's3',
					region: awsRegion,
					path: '/' + awsBucket + '/' + ch.identifier.value + '/' + ch.token,
					headers: {
						'Content-Type': 'text/plain;charset=UTF-8'
					},
					method: 'PUT',
					body: ch.keyAuthorization,
					signQuery: true
				},
				AWSCredentials
			);
			return request({
				// debug: true,
				method: 'PUT',
				url: 'https://' + signed.host + signed.path,
				headers: { 'Content-Type': 'text/plain;charset=UTF-8' },
				body: ch.keyAuthorization

			}).then(function(resp) {
				// console.log(resp.statusCode);
				if (200 !== resp.statusCode) {
					console.error(resp.statusCode);
					// console.error(resp.body);
					throw new Error('Could not PUT.');
				}
				return true;
			})
			.catch(function(err) {
				throw err;
				// return null;
			});
		},
		get: function(data) {
			console.log('List Key Auth URL');
			var ch = data.challenge;
			var signed = aws4.sign({
					host: awsHost,
					service: 's3',
					region: awsRegion,
					path: '/' + awsBucket + '/' + ch.identifier.value + '/' + ch.token,
					headers: {
						'Content-Type': 'text/plain;charset=UTF-8'
					},
					method: 'GET',
					signQuery: true
				},
				AWSCredentials
			);

			return request({
				// debug: true,
				method: 'GET',
				url: 'https://' + signed.host + signed.path,
				headers: { 'Content-Type': 'text/plain;charset=UTF-8' }
			}).then(function(resp) {
				if (200 === resp.statusCode) {
					return {
						keyAuthorization: resp.body
					};
				} else if (404 === resp.statusCode) {
					return null;
				}
				// wrong sign returns 403
				console.error(resp.statusCode);
				// console.error(resp.body);
				throw new Error('Could not GET');
			})
			.catch(function(err) {
				throw err;
				// return null;
			});

		},

		remove: function(data) {
			console.log('Remove Key Auth URL');
			var ch = data.challenge;

			var signed = aws4.sign({
					host: awsHost,
					service: 's3',
					region: awsRegion,
					path: '/' + awsBucket + '/' + ch.identifier.value + '/' + ch.token,
					method: 'DELETE',
					signQuery: true
				},
				AWSCredentials
			);
			return request({
				// debug: true,
				method: 'DELETE',
				url: 'https://' + signed.host + signed.path
			}).then(function(resp) {
				// console.log(resp.statusCode);
				if (204 !== resp.statusCode) {
					console.error(resp.statusCode);
					console.error(resp.body);
					throw new Error('Could not DELETE.');
				}
				return true;
			})
			.catch(function(err) {
				throw err;
				// return null;
			});

		}
	};
};
